COMMANDS := $(MAKEFILE_LIST)

lint: ## Lint code with shellcheck
	@echo "\033[1m  RUNNING SHELLCHECK\033[0m"
	@docker-compose --env-file .docker/.env -f .docker/docker-compose.yml run --rm shellcheck bin/indexer
	@echo "\033[32;1m晴 \033[1mSHELLCHECK OK\033[0m"

test: ## Run tests with bats
	@echo "\033[1m  RUNNING BATS\033[0m"
	@docker-compose --env-file .docker/.env -f .docker/docker-compose.test.yml run --rm bats -r /code/tests/unit
	@echo "\033[32;1m晴 \033[1mTESTS OK\033[0m"

.PHONY: lint test
.DEFAULT: all
