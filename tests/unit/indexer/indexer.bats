#!/usr/bin/env bats

load "$BATS_HELPER_PATH/bats-support/load.bash"
load "$BATS_HELPER_PATH/bats-assert/load.bash"
load "$BATS_HELPER_PATH/bats-file/load.bash"
load "./../helpers/assert"

setup() {
    # get the containing directory of this file
    # use $BATS_TEST_FILENAME instead of ${BASH_SOURCE[0]} or $0,
    # as those will point to the bats executable's location or the preprocessed file respectively
    DIR="$( cd "$( dirname "$BATS_TEST_FILENAME" )" >/dev/null 2>&1 && pwd )"
    # make executables in src/ visible to PATH
    PATH="$DIR/../../../bin:$PATH"
}

@test "Building index" {

  run indexer
  assert_success
  assert_file_exist build/shef-index
  assert_file_is "$(cat <<-EOF
[
  {
    "name": "captainquirk/task-recipe",
    "gitUrl": "git@gitlab.com:leonardmessier/shef/recipes/task-recipe.git",
    "dependencies": []
  },
  {
    "name": "captainquirk/git-recipe",
    "gitUrl": "git@gitlab.com:leonardmessier/shef/recipes/task-recipe.git",
    "dependencies": [
      "captainquirk/node-recipe"
    ]
  },
  {
    "name": "captainquirk/zsh-recipe",
    "gitUrl": "git@gitlab.com:leonardmessier/shef/recipes/zsh-recipe.git",
    "dependencies": [
      "captainquirk/powerline-recipe"
    ]
  }
]
EOF
  )"
}
